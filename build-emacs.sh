#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

readonly WRKDIR=$(pwd)
readonly TMPDIR=$(mktemp -d)

function cleanup() {
	rm -rf ${TMPDIR}
	cd ${WRKDIR}
}

trap cleanup EXIT

function main() {
	local tarfile="$1"
	local srcdir=${TMPDIR}/$(sed -e 's/\.tar\.gz//' <<<$(basename ${tarfile}))
	local builddir=${srcdir}/build

	tar -xvf ${tarfile} -C ${TMPDIR}

	cd ${srcdir}
	./autogen.sh

	mkdir -p ${builddir}
	cd ${builddir}

	${srcdir}/configure --prefix=/usr/local \
		--build=x86_64-pc-linux-gnu \
		--host=x86_64-pc-linux-gnu \
		--disable-silent-rules \
		--without-compress-install \
		--without-hesiod \
		--without-pop \
		--without-gameuser \
		--with-libgmp \
		--with-gpm \
		--with-json \
		--without-kerberos \
		--without-kerberos5 \
		--with-lcms2 \  --without-xml2 \
		--without-mailutils \
		--without-selinux \
		--with-gnutls \
		--with-libsystemd \
		--with-threads \
		--without-wide-int \
		--with-zlib \
		--with-sound=alsa \
		--with-x \
		--without-ns \
		--with-gconf \
		--with-gsettings \
		--with-toolkit-scroll-bars \
		--with-gif \
		--with-jpeg \
		--with-png \
		--with-rsvg \
		--with-tiff \
		--with-xpm \
		--without-imagemagick \
		--with-xft \
		--with-cairo \
		--without-harfbuzz \
		--without-libotf \
		--without-m17n-flt \
		--with-x-toolkit=gtk3 \
		--without-xwidgets

	make -j $(nproc)
	sudo make install
}

if [ "$#" -ne 1 ]; then
	cat <<EOF
usage: $(basename "$0") PATH/TO/EMACS/SRC.tar.gz
EOF
else
	main $1
fi
