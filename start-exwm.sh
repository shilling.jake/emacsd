#!/usr/bin/env sh


# Disable access control for the current user.
xhost +SI:localuser:$USER

# Make Java applications aware this is a non-reparenting window manager.
export _JAVA_AWT_WM_NONREPARENTING=1

## Run site init scripts. Usually not necessary.
if [ -d /etc/X11/xinit/xinitrc.d ] ; then
    for f in /etc/X11/xinit/xinitrc.d/?*.sh ; do
        [ -x "$f" ] && . "$f"
    done
    unset f
fi

xsetroot -cursor_name left_ptr
xset r rate 200 60

/usr/bin/xsettingsd &
/usr/bin/unclutter &
/usr/bin/numlockx on &
picom &

export VISUAL=emacsclient
export EDITOR="${VISUAL}"export GDK_CORE_DEVICE_EVENTS=1

emacs --daemon
emacsclient --eval "(load (expand-file-name \"exwm.el\" user-emacs-directory))"
exec dbus-launch --exit-with-session emacsclient -c
